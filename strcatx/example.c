/*
 * example.c
 */

#include <stdio.h>
#include "strcatx.h"

#define MAX 101 // [0..99], and str[100] is for '\0'

void test_fn() {    
    char str_dest[MAX] = "";
    strcatx(str_dest, sizeof(str_dest), 3, "Hello,", " World", "! It works.");
    printf("%s\n", str_dest);
}

int main(int argc, char * argv[]) {
    test_fn();
    return 0;
}