/*
 * strcatx.h
 * 
 * Applies variable list (va_list) to extend the strcat function. This new function 
 * strcatx (char * dest, int max_size, int num, ...) can append many strings to the 
 * end of the given string [dest].
 */

#ifndef STRCATX_H
#define STRCATX_H

#include <stdarg.h>  /* va_list */
#include <string.h>
#include <stdlib.h>


/*
 * @params:
 *      [dest]
 *          Pointer to the first position of the destionation string.
 *      [max_size]
 *          Maximum array size of [dest]. Note that the null-terminated char '\0' is also included.
 *      [num]
 *          Number of strings (in the va_list), that are going to be appended at the end of [dest] string.
 * 
 * @returns:
 *      The pointer to the first position of [dest] string after this function's finish.
 */
char * strcatx (char * dest, int max_size, int num, ...) {

    /* (1) Declare a va_list object. The in argument list above, va_list is represented by three dots (...). */
    va_list valist;

    /* (2) Set va_list's pointer to the LAST visible argument, i.e. [num]. The visible arguments are [dest],
     *     [max] and [num] in this case. The three dots (va_list) are placeholder for the UNvisible arguments. 
     *     va_start(va_list, lastArg) initializes the va_list and set pointer to [num]. By calling va_arg the 
     *     first time, the pointer is increased and refers to the first element of val_list. By every next call
     *     of va_arg, the pointer is increased accordingly and refers to the next va_list element.
     */
    va_start(valist, num);

    // Set other pointer for the string [dest]
    char * pDest = dest;

    // Set this pointer to the last position (where the null-terminated char '\0' is).
    pDest += strlen(dest); // If strlen(dest) == 0, pointer pDest refers to first the position in string.

    /* (3) Use va_arg(valist, [type]) to iterate/access to the elements of va_list */
    while (num > 0) {
        char * src = va_arg(valist, char *); // Source string. Every va_arg is of type (char *)
        int freeSpace = max_size - (1 + (int)strlen(dest)); // strlen() provides the length of string (number of chars)

        if (strlen(src) > freeSpace) {
            fprintf(stderr, "Error (strcatx): Overflow => The free space left in destionary string is not enough for source string.\n");
            exit(EXIT_FAILURE);
        }
        else {
            //strcat(dest, src); // Append string [src] to string [dest]
            while (*src) { // if every single char *src is not null (or '\0'), copy characater from [src] to [dest]
                *pDest = *src;
                pDest++;
                src++;
            }
        }
        num--;
    }

    //dest[strlen(dest)] = '\0'; // Null-terminated char: '\0' == 0 (End of string)
    pDest = '\0';

    /* (4) Clear all reserved memory for valist */
    va_end(valist);

    return dest;

}

#endif /* STRCATX_H */
