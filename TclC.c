 /*
 * This is a Tcl wrapper, written in C. It uses the C-API (tcl.h) to embed Tcl commands in C program.
 * For performance, some Tcl commands are completely replaced by plain C instructions.
 * 
 *  TclC.c
 */

#include <stdio.h>
#include "TclC.h"

//=====================================================================================================================================
/*
 * Main program
 * 
 * @params
 *      argc
 *      argv
 *          argv[0] = Name of this program (calling process)
 *          argv[1] = Name of the JSON file to read
 */
int main(int argc, char * argv[]) {

    Tcl_Interp * pTclInterp; // Reference to Tcp interpreter
    pTclInterp = Init_Tcl(argv[0]); // Create and init Tcl interpreter (context)
    
    Exec_Tcl_Command(pTclInterp, "puts \"Launch Tcl command(s) in C application.\"");

    Start_Pbgen(pTclInterp, argv[1]); // Start Tcl program

    Exit_Tcl(pTclInterp); // Remove Tcl interpreter and unload Tcl DLL from calling process

    return EXIT_SUCCESS;
}