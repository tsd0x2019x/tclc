 /*
 * This is a Tcl wrapper, written in C. It uses the C-API (tcl.h) to embed Tcl commands in C program.
 * For performance, some Tcl commands are completely replaced by plain C instructions.
 * 
 * Install Tcl on (linux) system:
 *      $ sudo apt-get install tcl tk expect
 *      $ sudo apt-get install tcllib           # for all tcl libraries like base64, json, etc.
 *      $ sudo apt-get install tcl-dev          # for tcl.h
 * 
 * Copy all tcl*.h files from "/usr/include/tcl8.6" or "/usr/include/tcl" to "/usr/include":
 *      $ sudo cp /usr/include/tcl/ /usr/include/
 * 
 * Optional shell wrapper for Tcl:
 *      $ sudo apt-get install rlwrap
 *
 * Compiler (GCC): 
 *    gcc -Wall -std=gnu99 -pedantic <source.c> -o <output> -ltcl
 *
 * Option -ltcl has to be used to link/include the <tcllib> library in C program.
 * 
 * Tcl C-API: http://www.tcl.tk/man/tcl8.5/TclLib/contents.htm
 * Tcl C-API Tcl_Obj:    https://www.tcl.tk/man/tcl/TclLib/Object.htm
 * Tcl C-API Dictionary: http://www.tcl-lang.org/man/tcl8.6/TclLib/DictObj.htm
 * 
 * Regular Expression: http://web.archive.org/web/20160308115653/http://peope.net/old/regex.html
 *                     https://linux.die.net/man/3/regcomp
 * Regular Expression: https://stackoverflow.com/questions/1085083/regular-expressions-in-c-examples
 */

#ifndef TCLC_H
#define TCLC_H

#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <regex.h>
#include <tcl.h>

#define STR_MAX_SIZE 512

/* This struct represents a bundle containing a [list] and a [string]. All elements of list are joined by using 
 * the Tcl command "join" and converted into a string content. This content is stored in [string].
 */
typedef struct {
    Tcl_Obj list;
    char string[STR_MAX_SIZE];
} Tcl_ListStringBundle;

//=====================================================================================================================================
/*
 * Checks whether a string matches a pattern (POSIX regular expression).
 * 
 * @return:
 *      Zero (0):    Success (Match)
 *      No-Zero (1): No match
 */
int Regex_Match (char * pattern, char * string) {
    regex_t regex;
    int reti, status;
    const int buff_size = 100;
    char errbuff[buff_size];

    /* Prepare and compile regular expression */
    reti = regcomp(&regex, pattern, REG_EXTENDED); // returns zero for a successful compilation or an error code for failure.

    if ( reti ) {
        fprintf(stderr, "Error (Regex_Match): Could not compile regex.\n");
        exit(EXIT_FAILURE);
    }

    /* Execute regular expression */
    reti = regexec(&regex, string, 0, NULL, 0); // returns zero for a successful match or REG_NOMATCH for failure.

    if ( !reti ) { // Match
        status = 0;
    }
    else if ( reti == REG_NOMATCH ) {
        status = 1; // No match
    }
    else {
        /* Raise error */
        regerror(reti, &regex, errbuff, buff_size); // buff_size == sizeof(errbuff)
        fprintf(stderr, "Error (Regex_Match): Could not execute regex => %s\n", errbuff);
        exit(EXIT_FAILURE);
    }
    /* Free compiled regular expression for next use of regex_t */
    regfree(&regex);

    return status;
}

/*
 * This function concatenates/combines the strings given in the va_list, which is represented by 
 * three dots (...), and then appends them to the end of string [dest].
 * 
 * @params:
 *      [dest]
 *          Pointer to the first position of the destionation string.
 *      [max_size]
 *          Maximum array size of [dest]. Note that the null-terminated char '\0' is also included.
 *      [num]
 *          Number of strings (in the va_list), that are going to be appended at the end of [dest] string.
 * 
 * @returns:
 *      The pointer to the first position of [dest] string after this function's finish.
 */
char * Str_Concat (char * dest, int max_size, int num, ...) {

    /* (1) Declare a va_list object. The variable list (va_list) is represented by three dots (...). */
    va_list valist;

    /* (2) Set va_list's pointer to the LAST visible argument, i.e. [num]. The visible arguments are [dest],
     *     [max] and [num] in this case. The three dots (va_list) are placeholder for the UNvisible arguments.
     *     va_start(va_list, lastArg) initializes the va_list and set pointer to [num]. By calling va_arg the 
     *     first time, the pointer is increased and refers to the first element of val_list . By every next call
     *     of va_arg, the pointer is increased accordingly and refers to the next va_list element.
     */
    va_start(valist, num);

    // Set other pointer for the string [dest]
    char * pDest = dest;

    // Set this pointer to the last position (where the null-terminated char '\0' is).
    pDest += strlen(dest); // If strlen(dest) == 0, pointer pDest refers to first the position in string.

    /* (3) Use va_arg(valist, [type]) to iterate/access to the elements of va_list */
    while (num > 0) {
        char * src = va_arg(valist, char *); // Source string. Every va_arg is of type (char *)
        int free_space = max_size - (1 + (int)strlen(dest)); // strlen() provides the length of string (number of chars)

        if (strlen(src) > free_space) {
            fprintf(stderr, "Error (Str_Concat): Overflow => The free space left in destionary string is not enough for source string.\n");
            exit(EXIT_FAILURE);
        }
        else {
            //strcat(dest, src); // Append string [src] to string [dest]
            while (*src) { // if every single char *src is not null ('\0'), copy characater from [src] to [dest]
                *pDest = *src;
                pDest++;
                src++;
            }
        }
        num--;
    }

    //dest[strlen(dest)] = '\0'; // Null-terminated char: '\0' == 0 (End of string)
    pDest = '\0';

    /* (4) Clear all reserved memory for valist */
    va_end(valist);

    return &dest[0];
}

/*
 * Inserts a string [src] to the beginning of string [dest]. Argument [dest_max_size] indicates the maximum number
 * of (available) bytes of [dest] string.
 */
char * Str_InsertFirst (char * src, char * dest, int dest_max_size) {
    int free_space = dest_max_size - (1 + (int)strlen(dest)); // strlen provides the number of char, 1 indicates the null-terminated byte '\0'
    int src_len = strlen(src);

    if (src_len > free_space) {
        fprintf(stderr, "Error (strcatx): Overflow => The free space left in destionary string is not enough for source string.\n");
        exit(EXIT_FAILURE);
    }

    for (int i = (strlen(dest)-1); i >= 0; i--) { dest[i+src_len] = dest[i]; }
    for (int i = 0; i < src_len; i++) { dest[i] = src[i]; }

    return dest;
}

//=====================================================================================================================================
/*
 * Return the length of a Tcl list
 */
int Get_Tcl_ListLength (Tcl_Interp * pTclInterp, Tcl_Obj * list) {
    int len;
    int status = Tcl_ListObjLength(pTclInterp, list, &len);
    if (status != TCL_OK) {
        fprintf(stderr, "Error (Get_Tcl_ListLength): Could not determine list length => Details: %s\n", Tcl_GetStringResult(pTclInterp));
        exit(EXIT_FAILURE);
    }
    return len;
}

/*
 * Return a reference to the element at index [index] in list
 */
Tcl_Obj * Get_Tcl_ListElement (Tcl_Interp * pTclInterp, Tcl_Obj * list, int index) {
    if ( index < 0 || index >= Get_Tcl_ListLength(pTclInterp, list) ) {
        fprintf(stderr, "Error (Get_Tcl_ListElement): List index (%d) out of range.\n", index);
        exit(EXIT_FAILURE);
    }
    Tcl_Obj * obj;
    // Get element at [index] from list and store the reference to this element in [obj]
    int status = Tcl_ListObjIndex(pTclInterp, list, index, &obj);   
    if (status != TCL_OK) {
        fprintf(stderr, "Error (Get_Tcl_ListElement): Could not get element at index %d. List is not valid => Details: %s\n", index, Tcl_GetStringResult(pTclInterp));
        exit(EXIT_FAILURE);
    }    
    return obj;
}

/*
 * This function is an implementation for the Tcl "join" command that converts a Tcl list 
 * into a string. Every list element is separated by the given [separator]. The final string
 * representation for list is stored in [result].
 */
void Join_Tcl_List (Tcl_Interp * pTclInterp, Tcl_Obj * list, char * result, char * separator) {
    int len = Get_Tcl_ListLength(pTclInterp, list);
    Tcl_Obj * elem;
    char * str;
    for (int i = 0; i < (len-1); i++) {
        elem = Get_Tcl_ListElement(pTclInterp, list, i);
        str = Tcl_GetString(elem); // Get string representation of the Tcl object [elem]
        Str_Concat(result, STR_MAX_SIZE, 2, str, separator);
    }
    elem = Get_Tcl_ListElement(pTclInterp, list, len-1);
    str = Tcl_GetString(elem);
    Str_Concat(result, STR_MAX_SIZE, 1, str);
}

/*
 * Remove Tcl interpreter and unload Tcl DLL from calling process
 */
void Exit_Tcl (Tcl_Interp * pTclInterp) {
    Tcl_DeleteInterp(pTclInterp);
    Tcl_Finalize(); // Unload the Tcl DLL from process
}

/*
 * Call/Run a Tcl command
 */
void Exec_Tcl_Command (Tcl_Interp * pTclInterp, char * tcl_command) {
    int status = Tcl_Eval(pTclInterp, tcl_command);
    if (status != TCL_OK) {
        fprintf(stderr, "Error: Tcl command %s => Message: %s\n", tcl_command, Tcl_GetStringResult(pTclInterp));
        exit(EXIT_FAILURE);
    }
}

/*
 * Create and initialize an Tcl interpreter (context)
 */
Tcl_Interp * Init_Tcl (char * argv0) {

    // Initialize the name of the binary file containing the (calling) application
    Tcl_FindExecutable(argv0);   

    // Create pointer to Tcl interpreter (Tcl context) for this application.
    Tcl_Interp * pTclInterp = Tcl_CreateInterp();

    // Error handling
    if (pTclInterp == NULL) {
        fprintf(stderr, "Error (Tcl_CreateInterp): Tcl interpreter could not be created.\n");
        exit(EXIT_FAILURE);
    }

    // Initialize Tcl interpreter
    if (Tcl_Init(pTclInterp) != TCL_OK) {
        fprintf(stderr, "Error (Tcl_Init): %s\n", Tcl_GetStringResult(pTclInterp));
        exit(EXIT_FAILURE);
    }

    return pTclInterp;
}

/*
 * Require/Import a package with name and version number
 * @params:
 *      [exact]
 *          Non-zero means that only the version given in <version> is acceptable. 
 *          Zero means that newer versions than [version] is also acceptable as 
 *          long as they have the same major version number as [version].
 */
int Require_Tcl_Package (Tcl_Interp * pTclInterp, const char * name, const char * version, int exact) {
    Exec_Tcl_Command(pTclInterp, "package require json");
    const char * tclInterpResult = Tcl_GetStringResult(pTclInterp);
    
    char current_version[10] = "";
    strcpy(current_version, tclInterpResult);

    const char * required_version = Tcl_PkgRequire(pTclInterp, name, version, exact); // returns a pointer to the version string (of the package)
                                                                                      // that is provided in the interpreter and may be different
                                                                                      // than <version> given in the argument list.
    if (required_version == NULL) {
        if (current_version == NULL) {
            fprintf(stderr, "Error (Require_Tcl_package): Package \"%s\" is not installed. Version \"%s\" could not be found.\n", name, version);
            return TCL_ERROR; // 1
        }
        else {;
            required_version = Tcl_PkgRequire(pTclInterp, name, current_version, exact);
            printf("Warning (Require_Tcl_package): Package \"%s %s\" is required, but only \"%s %s\" is found. ", name, version, name, current_version);
            printf("We use the currently installed package \"%s %s\" instead.\n", name, current_version);
            return TCL_RETURN; // 2
        }
    }
    else {
        return TCL_OK; // 0
    }
}

//=====================================================================================================================================
/*
 * Open the given JSON file, read and parse its content. Finally, return an dictionary object 
 * that contains the parsed content.
 */
Tcl_Obj Open_Json2Dict (Tcl_Interp * pTclInterp, char * filename) {
    printf("open_json2dict...\n");
    if (filename == NULL) {
        fprintf(stderr, "Error (Open_Json2Dict): There is no file name.\n");
        exit(EXIT_FAILURE);
    }
    /*
     * set f [open [subst $filename]]
     * set d [::json::json2dict [read $f]]
     * close $f
     * return $d
     */
    char tcl_command[STR_MAX_SIZE] = "";    
    Str_Concat(tcl_command, STR_MAX_SIZE, 3, "set f [open ", filename, "]");
    Exec_Tcl_Command(pTclInterp, tcl_command); // set f [open [subst $filename]]

    Exec_Tcl_Command(pTclInterp, "set d [::json::json2dict [read $f]]");
    Tcl_Obj * d = Tcl_GetVar2Ex(pTclInterp, "d", NULL, TCL_GLOBAL_ONLY); // Get object Dictionary d
    //printf("DEBUG: d->typePtr->name: %s\n", d->typePtr->name); // Tcl_ObjType of d
    //printf("DEBUG: d: %s\n", Tcl_GetString(d)); // String content of d

    Exec_Tcl_Command(pTclInterp, "close $f");

    return *d;
}

/*
 * Reads from an dictionary object, converts its content to JSON structure and returns it as a list.
 * Furthermore, the list elements are combined (joined) into a string and separated by commata (,)
 * followed by newline (\n).
 */
Tcl_ListStringBundle Write_Dict2Json (Tcl_Interp * pTclInterp, Tcl_Obj dictionary) {
    printf("dict2jsonlist...\n");
    /*
     * dict for {key value} $dictionary {
     *    if {[string match {\[*\]} $value]} {
     *        lappend Result "\"$key\": $value"
     *    } elseif {![catch {dict size $value}]} {  ; # [dict size] returns number of key/value mappings
     *        lappend Result "\"$key\": [dict2json $value]"
     *    } else {
     *        lappend Result "\"$key\": \"$value\"" ;# join converts a list into a string
     *    }
     * }
     * return "\{[join $Result ",\n"]\}"
     */

    Tcl_DictSearch search;
    Tcl_Obj *key, *value, *lst;
    Tcl_ListStringBundle bundle;
    int done, iSize;
    char result[STR_MAX_SIZE] = "";

    // Starts an iteration across all {key value} pairs in the given dictionary
    int status = Tcl_DictObjFirst(pTclInterp, &dictionary, &search, &key, &value, &done);

    if (status != TCL_OK) {
        fprintf(stderr, "Error (Write_Dict2Json): Iteration is aborted. Argument [dictionary] is not valid.\n");
        exit(TCL_ERROR);
    }

    // Initializes an empty list
    lst = Tcl_NewListObj(0, NULL); 

    // Check if the list object is "shared", i.e. if it's used and referenced by many procedures. An object is
    // shared if its [refCount] value is no-zero. This number indicates how many procedures currently refer to 
    // it. In this case, a duplicate of this object must be created by using Tcl_DuplicateObj. Otherwise, if
    // [refCount] is zero, this object is not shared, so the calling procedure can access to and modify it 
    // directly. The function Tcl_IsShared(obj) indicates if the object is shared or not. If one skips this check, 
    // the error "..called with shared object" might be raised and abort program.
    if (Tcl_IsShared(lst)) {  lst = Tcl_DuplicateObj(lst); }

    for ( ; !done ; Tcl_DictObjNext(&search, &key, &value, &done) ) {
        char * strKey = Tcl_GetString(key);
        char * strVal = Tcl_GetString(value);
        char strInp[STR_MAX_SIZE] = "";

        Tcl_DictObjSize(pTclInterp, value, &iSize); // Update [iSize] with number of key/value pairs contained in [value].
        Tcl_Obj * elem = Tcl_NewObj(); // Create new Tcl object to contain string [strInp]

        // To escape left brace, as "\[", one must escape the baskslash first, i.e. "\\". It results to "\\[".
        if (Tcl_StringMatch(strVal, "\\[*\\]")) { // Pattern for [*]
            Str_Concat(strInp, STR_MAX_SIZE, 4, "\"", strKey, "\": ", strVal);
        }
        else if (iSize > 0 && iSize < 10000000) { // If there are many key/value pairs/mapping within [strVal]. 
            // The limit 10000000 is workaround for the problem case: when there is no mapping, [iSize] sometimes does not get 0, 
            // but a very big unsigned value like 14321542 => Error.
            Tcl_ListStringBundle newBundle = Write_Dict2Json(pTclInterp, *value); // Recursion
            Str_Concat(strInp, STR_MAX_SIZE, 4, "\"", strKey, "\": ", newBundle.string);
        }
        else {
            Str_Concat(strInp, STR_MAX_SIZE, 5, "\"", strKey, "\": \"", strVal, "\"");
        }
        // Insert the character bytes (char *) of [strInp] to object [elem].
        Tcl_AppendToObj(elem, strInp, -1); // -1: Use all bytes (char *) stored in [strInp]
        // Insert [elem] to the end of list
        Tcl_ListObjAppendElement(pTclInterp, lst, elem);
    }

    // return "\{[join $Result ",\n"]\}"
    Join_Tcl_List(pTclInterp, lst, result, ",\n");
    Str_Concat(result, STR_MAX_SIZE, 1, "}");
    Str_InsertFirst("{", result, STR_MAX_SIZE);

    bundle.list = *lst;
    strcpy(bundle.string, result);    

    return bundle;
}

//=====================================================================================================================================
/*
 *
 */
void Gen_Vhdl_Parset (Tcl_Interp * pTclInterp, Tcl_Obj * input_json_dic) {
    printf("gen_vhdl_parset\n");

    printf("DEBUG: input_json_dict:\n%s\n", Tcl_GetString(input_json_dic));
    
    Tcl_ListStringBundle bundle = Write_Dict2Json(pTclInterp, *input_json_dic);

    printf("\nDEBUG (bundle.list): \n%s\n", Tcl_GetString(&(bundle.list)));
    printf("\nDEBUG (bundle.string): \n%s\n", bundle.string);
}

/*
 * @params:
 *      [pTclInterp]
 *          Pointer to the (current) Tcl interpreter
 *      [dict]
 *          Pointer to a dictionary object (Tcl_DicObj) 
 */
void Gen_Vhdl (Tcl_Interp * pTclInterp, Tcl_Obj * dict) {
    Gen_Vhdl_Parset(pTclInterp, dict);
    //Gen_Vhdl_Parbank(pTclInterp, dic);
    //Gen_Vhdl_Records(pTclInterp, dic);
}

/*
 * Read Json file and start generating Vhdl code
 */
void Start_Pbgen (Tcl_Interp * pTclInterp, char * json_file_name) {
    /*
     * package require json     * 
     * set dic  [open_json2dict [subst AXIL_HWIO.json]]
     * gen_vhdl [subst $dic]
     */
    if (Require_Tcl_Package(pTclInterp, "json", "1.3.4", 0) == TCL_ERROR) { // package require json
        exit(EXIT_FAILURE);
    }

    Tcl_Obj dic = Open_Json2Dict(pTclInterp, json_file_name); // set dic  [open_json2dict [subst AXIL_HWIO.json]]
    Gen_Vhdl(pTclInterp, &dic); // gen_vhdl [subst $dic]
}



#endif /* TCLC_H */