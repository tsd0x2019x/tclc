/*
 * Test stringx.h
 */

#include <stdio.h>
#include "stringx.h"

int main (int argc, char * argv[]) {
    char string[] = "  Hello World! ";
    char output[30] = ""; // 29 char plus null-terminated '\0'
    char * newString;

    printf("Original: %s\n", string);

    // Trimming
    Trim_String(string, output, sizeof(output));
    printf("Trimmed : %s\n", output);

    //memcpy(output, "", sizeof(output)); // clear string
    memcpy(string, output, strlen(string)); // copy trimmed string from [output] to [string]

    // Concatenation
    Str_Concat(output, sizeof(output), 2, " Hallo", " Welt!");
    printf("Concated (append): %s\n", output);

    // Duplication
    newString = Duplicate_String(string);
    printf("\nDuplicate: %s\n", newString);
    newString[1] = 'A'; // change character
    printf("Duplicate (after change): %s\n", newString);
    printf("Original: %s\n", string);
    free(newString);
}