CC 		= /usr/bin/gcc
CFLAGS  = -Wall -Werror -std=gnu99 -pedantic
LDFLAGS = -ltcl

SRC_H = TclC.h
SRC_C = TclC.c
BIN = TclC

TclC: $(SRC_C) $(SRC_H)
		$(CC) $(CFLAGS) $(SRC_C) -o $(BIN) $(LDFLAGS)


.PHONY: clean
clean:
		rm -f $(BIN)