/*
 * Embed Tcl in C application
 * 
 * sudo apt-get install tcl tk expect
 * sudo apt-get install rlwrap
 * sudo apt-get install tcllib  # for all tcl libraries like base32, base64, json,  etc.
 * sudo apt-get install tcl-dev # for tcl.h
 *
 * Compile: 
 *    gcc -Wall -std=gnu99 -pedantic <quelldatei.c> -o <output.exe> -ltcl
 *
 *    -std=c90 -pedantic
 *    -std=c89 -pedantic
 *    -std=c99 -pedantic
 *    -std=gnu99 -pedantic
 *
 * Option -ltcl wird verwendet, um die libtcl Bibliothek von C einzubinden.
 */

#include <stdio.h>
#include <tcl.h>

int main(int argc, char * argv[]) {

    printf("Running Tcl command(s) in C application...\n");
    
    /* (1) Initialize the Tcl library */
    Tcl_FindExecutable(argv[0]);

    char * tcl_command = "puts \"Hello World!\"";
    int status = 0;

    /* (2) Create pointer to Tcl interpreter (Tcl context) */
    Tcl_Interp * pTclInterp = Tcl_CreateInterp();

    /* (3) Initialize the Tcl interpreter */
    if (Tcl_Init(pTclInterp) != TCL_OK) {
        fprintf(stderr, "Tcl_Init error: %s\n", Tcl_GetStringResult(pTclInterp));
        return -1;
    }

    /* (4) Execute Tcl command */
    status = Tcl_Eval(pTclInterp, tcl_command);
    //status = Tcl_EvalFile(pTclInterp, "start_pbgen.tcl"); // Run a tcl file

    /* (5) Remove Tcl interpreter */
    Tcl_DeleteInterp(pTclInterp);
    
    /* (6) Unload the Tcl DLL from process */
    Tcl_Finalize();

    printf("Status code (Tcl_Eval): %d\n", status);
    printf("Done!\n");

    return 0;
}